<div align="center">
  <h1>deep-object-diff-array</h1>

  ❄️

  Deep diff two JavaScript Objects
</div>

<hr />

[![Build Status](https://img.shields.io/gitlab/pipeline/awcjack/deep-object-diff-array/master)](https://gitlab.com/awcjack/deep-object-diff-array/-/pipelines)
[![Code Coverage](https://img.shields.io/gitlab/coverage/awcjack/deep-object-diff-array/master)](https://awcjack.gitlab.io/deep-object-diff-array/)
[![version](https://img.shields.io/npm/v/deep-object-diff-array)](https://www.npmjs.com/package/deep-object-diff-array)
[![downloads](https://img.shields.io/npm/dm/deep-object-diff-array.svg?style=flat-square)](http://npm-stat.com/charts.html?package=deep-object-diff-array&from=2021-10-17)
[![MIT License](https://img.shields.io/npm/l/deep-object-diff-array.svg?style=flat-square)](https://gitlab.com/awcjack/deep-object-diff-array/-/blob/master/LICENSE)
[![PRs Welcome](https://img.shields.io/badge/PRs-welcome-brightgreen.svg?style=flat-square)](http://makeapullrequest.com)

A small library that can deep diff two JavaScript Objects, including nested structures of arrays and objects.

## Installation
`yarn add deep-object-diff-array`

## Functions available:
 - [`addedDiff(original, updatedObj, set)`](#addeddiff)
 returns only the values added to the updated object

 - [`deletedDiff(original, updatedObj, set)`](#deleteddiff)
 returns only the values deleted in the updated object

 - [`updatedDiff(original, updatedObj, set)`](#updateddiff)
 returns only the values that have been changed in the updated object

 - [`detailedDiff(original, updatedObj, set)`](#detaileddiff)
 returns an object with the added, deleted and updated differences

## Importing

ES6 / Babel:
``` js
import { addedDiff, deletedDiff, updatedDiff, detailedDiff } from 'deep-object-diff-array';
```

ES5:
``` js
const { addedDiff, deletedDiff, detailedDiff, updatedDiff } = require("deep-object-diff-array");

// OR

const addedDiff = require("deep-object-diff-array").addedDiff;
const deletedDiff = require("deep-object-diff-array").deletedDiff;
const detailedDiff = require("deep-object-diff-array").detailedDiff;
const updatedDiff = require("deep-object-diff-array").updatedDiff;
```

## Usage:
### Set flag:
Set means consider array element is unique and the order doesn't matter (updatedDiff array always empty in Set)
### `addedDiff`:
```js
const lhs = {
  foo: {
    bar: {
      a: ['a', 'b'],
      b: 2,
      c: ['x', 'y'],
      e: 100 // deleted
    }
  },
  buzz: 'world'
};

const rhs = {
  foo: {
    bar: {
      a: ['a'], // index 1 ('b')  deleted
      b: 2, // unchanged
      c: ['y', 'x', 'z'], // 'z' added and reordered x, y
      d: 'Hello, world!' // added
    }
  },
  buzz: 'fizz' // updated
};

console.log(addedDiff(lhs, rhs));

/*
{
  "foo": {
    "bar": {
      "c": {
        "after": [
          "z"
        ]
      },
      "d": {
        "after": "Hello, world!"
      }
    }
  }
}
*/

console.log(addedDiff(lhs, rhs, false)); // non Set
/*
{
  "foo": {
    "bar": {
      "c": {
        "after": [
          {
            "content": "z",
            "indices": [
              2 // element added in index 2 of array
            ]
          }
        ]
      },
      "d": {
        "after": "Hello, world!" // normal add new field in object
      }
    }
  }
}
*/
```

### `deletedDiff`:
```js
const lhs = {
  foo: {
    bar: {
      a: ['a', 'b'],
      b: 2,
      c: ['x', 'y'],
      e: 100 // deleted
    }
  },
  buzz: 'world'
};

const rhs = {
  foo: {
    bar: {
      a: ['a'], // index 1 ('b')  deleted
      b: 2, // unchanged
      c: ['y', 'x', 'z'], // 'z' added and reordered x, y
      d: 'Hello, world!' // added
    }
  },
  buzz: 'fizz' // updated
};

console.log(deletedDiff(lhs, rhs));

/*
{
  "foo": {
    "bar": {
      "a": {
        "before": [
          "b"
        ]
      },
      "e": {
        "before": 100
      }
    }
  }
}
*/

console.log(deletedDiff(lhs, rhs, false));

/*
{
  "foo": {
    "bar": {
      "a": {
        "before": [
          {
            "content": "b",
            "indices": [
              1 // removed element from array index 1
            ]
          }
        ]
      },
      "e": {
        "before": 100 // normal remove field in object
      }
    }
  }
}
*/
```

### `updatedDiff`:
```js
const lhs = {
  foo: {
    bar: {
      a: ['a', 'b'],
      b: 2,
      c: ['x', 'y'],
      e: 100 // deleted
    }
  },
  buzz: 'world'
};

const rhs = {
  foo: {
    bar: {
      a: ['a'], // index 1 ('b')  deleted
      b: 2, // unchanged
      c: ['y', 'x', 'z'], // 'z' added and reordered x, y
      d: 'Hello, world!' // added
    }
  },
  buzz: 'fizz' // updated
};

console.log(updatedDiff(lhs, rhs));

/*
{
  "buzz": {
    "before": "world",
    "after": "fizz"
  }
}
*/

console.log(updatedDiff(lhs, rhs, false));

/*
{
  "foo": {
    "bar": {
      "c": {
        "after": [ // first element x and first element y swapped
          {
            "content": "y",
            "counter": 0,
            "newIndex": 0,
            "oldIndex": 1
          },
          {
            "content": "x",
            "counter": 0,
            "newIndex": 1,
            "oldIndex": 0
          }
        ]
      }
    }
  },
  "buzz": {
    "before": "world",
    "after": "fizz"
  }
}
*/
```

### `detailedDiff`:
```js
const lhs = {
  foo: {
    bar: {
      a: ['a', 'b'],
      b: 2,
      c: ['x', 'y'],
      e: 100 // deleted
    }
  },
  buzz: 'world'
};

const rhs = {
  foo: {
    bar: {
      a: ['a'], // index 1 ('b')  deleted
      b: 2, // unchanged
      c: ['y', 'x', 'z'], // 'z' added and reordered x, y
      d: 'Hello, world!' // added
    }
  },
  buzz: 'fizz' // updated
};

console.log(detailedDiff(lhs, rhs));

/*
{
  "added": {
    "foo": {
      "bar": {
        "c": {
          "after": [
            "z"
          ]
        },
        "d": {
          "after": "Hello, world!"
        }
      }
    }
  },
  "deleted": {
    "foo": {
      "bar": {
        "a": {
          "before": [
            "b"
          ]
        },
        "e": {
          "before": 100
        }
      }
    }
  },
  "updated": {
    "buzz": {
      "before": "world",
      "after": "fizz"
    }
  }
}
*/

console.log(detailedDiff(lhs, rhs, false));
/*
{
  "added": {
    "foo": {
      "bar": {
        "c": {
          "after": [
            {
              "content": "z",
              "indices": [
                2
              ]
            }
          ]
        },
        "d": {
          "after": "Hello, world!"
        }
      }
    }
  },
  "deleted": {
    "foo": {
      "bar": {
        "a": {
          "before": [
            {
              "content": "b",
              "indices": [
                1
              ]
            }
          ]
        },
        "e": {
          "before": 100
        }
      }
    }
  },
  "updated": {
    "foo": {
      "bar": {
        "c": {
          "after": [
            {
              "content": "y",
              "counter": 0,
              "newIndex": 0,
              "oldIndex": 1
            },
            {
              "content": "x",
              "counter": 0,
              "newIndex": 1,
              "oldIndex": 0
            }
          ]
        }
      }
    },
    "buzz": {
      "before": "world",
      "after": "fizz"
    }
  }
}
*/
```


## License

MIT
