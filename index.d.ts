export function addedDiff (originalObj: object, updatedObj: object, set: boolean): object

export function deletedDiff (originalObj: object, updatedObj: object, set: boolean): object

export function updatedDiff (originalObj: object, updatedObj: object, set: boolean): object

export function detailedDiff (originalObj: object, updatedObj: object, set: boolean): object
