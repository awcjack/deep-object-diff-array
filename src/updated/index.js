import _ from "lodash"
import { isDate, isEmpty, isObject, properObject } from '../utils';

const arrayUpdate = (lhs, rhs) => {
  const leftArray = _.cloneDeep(lhs)
  const rightArray = _.cloneDeep(rhs)
  const addedFields = _.uniq(_.difference(rhs, lhs))
  const deletedFields = _.uniq(_.difference(lhs, rhs))
  _.pullAll(leftArray, deletedFields)
  _.pullAll(rightArray, addedFields)
  if (_.isEqual(leftArray, rightArray)) {
    return undefined
  } else {
    const allKeys = _.uniq(rightArray)
    const allKeysCounter = _.reduce(allKeys, (acc, curr) => (acc[curr] = 0, acc), {})
    const changesArray = []
    for (let i = 0; i < leftArray.length; i++) {
      if (!_.isEqual(leftArray[i], rightArray[i])) {
        let newIndex = _.findIndex(rhs, value => _.isEqual(value, rightArray[i]))
        for (let j = 0; j < allKeysCounter[isObject(rightArray[i]) ? JSON.stringify(rightArray[i]) : rightArray[i]]; j++) {
          newIndex = _.findIndex(rhs, value => _.isEqual(value, rightArray[i]), newIndex + 1)
        }
        let oldIndex = _.findIndex(lhs, value => _.isEqual(value, rightArray[i]))
        for (let j = 0; j < allKeysCounter[isObject(rightArray[i]) ? JSON.stringify(rightArray[i]) : rightArray[i]]; j++) {
          oldIndex = _.findIndex(lhs, value => _.isEqual(value, rightArray[i]), oldIndex + 1)
        }
        changesArray.push({
          content: rightArray[i],
          counter: allKeysCounter[rightArray[i]],
          newIndex,
          oldIndex,
        })
      }
      allKeysCounter[isObject(rightArray[i]) ? JSON.stringify(rightArray[i]) : rightArray[i]]++
    }
    return changesArray
  }
}

const updatedDiff = (lhs, rhs, set = true) => {
  if (_.isEqual(lhs, rhs)) return {};
  if (Array.isArray(lhs) && Array.isArray(rhs)) {
    if (set) return {}
    const diff = arrayUpdate(lhs, rhs, set)
    if (_.isEmpty(diff)) return {}
    return { after: diff }
  }
  if (!isObject(lhs) || !isObject(rhs)) return rhs;

  const l = properObject(lhs);
  const r = properObject(rhs);

  if (isDate(l) || isDate(r)) {
    if (l.valueOf() == r.valueOf()) return {};
    return r;
  }

  return Object.keys(r).reduce((acc, key) => {
    if (l.hasOwnProperty(key)) {
      const difference = updatedDiff(l[key], r[key], set);

      if (isObject(difference) && isEmpty(difference) && !isDate(difference)) return acc;

      if ((isObject(difference) && !isDate(difference)) || (isObject(l[key]) && !isDate(l[key]))) {
        acc[key] = difference
        return acc
      }
      acc[key] = { before: l[key], after: difference }
      return acc;
    }

    return acc;
  }, {});
};

export default updatedDiff;
