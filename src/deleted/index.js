import _ from "lodash"
import { isEmpty, isObject, properObject, isDate, arrayDiff } from '../utils';

const deletedDiff = (lhs, rhs, set = true) => {
  if (_.isEqual(lhs, rhs)) return {}
  if (Array.isArray(lhs) && Array.isArray(rhs)) {
    const difference = arrayDiff(rhs, lhs, set)
    if (_.isEmpty(difference)) return {}
    return { before: difference }
  }
  if (!isObject(lhs) || !isObject(rhs) || isDate(lhs)) return {};

  const l = properObject(lhs);
  const r = properObject(rhs);

  return Object.keys(l).reduce((acc, key) => {
    if (r.hasOwnProperty(key)) {
      const difference = deletedDiff(l[key], r[key], set);

      if (isObject(difference) && isEmpty(difference)) return acc;

      acc[key] = difference
      return acc
    }
    if (isObject(l[key]) && !isDate(l[key]) && r[key] === undefined) {
      if (Array.isArray(l[key])) {
        acc[key] = { before: l[key] }
        return acc
      }
      const difference = deletedDiff(l[key], {}, set);
      if (isObject(difference) && isEmpty(difference)) return acc;
      acc[key] = difference
      return acc
    }
    acc[key] = { before: l[key] }
    return acc
  }, {});
};

export default deletedDiff;
