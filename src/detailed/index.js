import addedDiff from '../added';
import deletedDiff from '../deleted';
import updatedDiff from '../updated';

const detailedDiff = (lhs, rhs, set = true) => ({
  added: addedDiff(lhs, rhs, set),
  deleted: deletedDiff(lhs, rhs, set),
  updated: updatedDiff(lhs, rhs, set),
});

export default detailedDiff;
