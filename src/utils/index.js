import _ from "lodash"

export const isDate = d => d instanceof Date;
export const isEmpty = o => Object.keys(o).length === 0;
export const isObject = o => o != null && typeof o === 'object';
export const properObject = o => isObject(o) && !o.hasOwnProperty ? { ...o } : o;
export const arrayDiff = (lhs, rhs, set) => {
  const extraFields = _.uniqWith(_.difference(rhs, lhs, _.isEqual), _.isEqual)
  if (extraFields.length === 0) {
    return undefined
  }
  if (set) {
    return extraFields
  }
  const extraFieldsIndices = _.map(extraFields, o => ({
    content: o,
    indices: []
  }))
  for (let i = 0; i < rhs.length; i++) {
    const index = _.findIndex(extraFields, o => _.isEqual(o, rhs[i]))
    if (index !== -1) {
      extraFieldsIndices[index].indices.push(i)
    }
  }
  return extraFieldsIndices
}