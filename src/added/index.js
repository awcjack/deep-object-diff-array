import _ from "lodash"
import { isEmpty, isObject, properObject, isDate, arrayDiff } from '../utils';

const addedDiff = (lhs, rhs, set = true) => {
  if (_.isEqual(lhs, rhs)) return {}
  if (Array.isArray(lhs) && Array.isArray(rhs)) {
    const difference = arrayDiff(lhs, rhs, set)
    if (_.isEmpty(difference)) return {}
    return { after: difference }
  }
  if (!isObject(lhs) || !isObject(rhs) || isDate(rhs)) return {};

  const l = properObject(lhs);
  const r = properObject(rhs);

  return Object.keys(r).reduce((acc, key) => {
    if (l.hasOwnProperty(key)) {
      const difference = addedDiff(l[key], r[key], set);

      if (isObject(difference) && isEmpty(difference)) return acc;

      acc[key] = difference;
      return acc;
    }
    if (isObject(r[key]) && !isDate(r[key]) && l[key] === undefined) {
      if (Array.isArray(r[key])) {
        acc[key] = { after: r[key] };
        return acc;
      }
      const difference = addedDiff({}, r[key], set);
      if (isObject(difference) && isEmpty(difference)) return acc;
      acc[key] = difference;
      return acc;
    }
    acc[key] = { after: r[key] };
    return acc;
  }, {});
};

export default addedDiff;
