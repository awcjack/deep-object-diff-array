import addedDiff from './added';
import deletedDiff from './deleted';
import updatedDiff from './updated';
import detailedDiff from './detailed';

export {
  addedDiff,
  deletedDiff,
  updatedDiff,
  detailedDiff,
};
