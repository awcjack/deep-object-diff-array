## [1.2.3](https://gitlab.com/awcjack/deep-object-diff-array/compare/v1.2.2...v1.2.3) (2022-01-14)


### Bug Fixes

* rename simpleArray to set ([d424e4d](https://gitlab.com/awcjack/deep-object-diff-array/commit/d424e4d2a05b9693f54feac55d5ddaafe7eb5cae))

## [1.2.2](https://gitlab.com/awcjack/deep-object-diff-array/compare/v1.2.1...v1.2.2) (2021-11-10)


### Bug Fixes

* change spread syntax, array diff use _.equal for comparing, add large object test case ([e0f83c6](https://gitlab.com/awcjack/deep-object-diff-array/commit/e0f83c6b5d1a70856e0d2ffa215fe6ccb50494a1))

## [1.2.1](https://gitlab.com/awcjack/deep-object-diff-array/compare/v1.2.0...v1.2.1) (2021-10-17)


### Bug Fixes

* update coverage url ([4d04b64](https://gitlab.com/awcjack/deep-object-diff-array/commit/4d04b6436b28d245638c0b9f598c2e8cefd658f5))

# [1.2.0](https://gitlab.com/awcjack/deep-object-diff-array/compare/v1.1.0...v1.2.0) (2021-10-17)


### Bug Fixes

* add oldIndex in update ([01ad236](https://gitlab.com/awcjack/deep-object-diff-array/commit/01ad23634411824efdb5b89a16fb641b8c6cb53e))
* handle reorder and add simple array option ([2a15a80](https://gitlab.com/awcjack/deep-object-diff-array/commit/2a15a80538c56d2f9cee4cb9ba0907a703e83f1e))
* preserve array in detailed diff & testcase in detailed diff ([a251375](https://gitlab.com/awcjack/deep-object-diff-array/commit/a251375892a82290557ccc113af527673a8b02b2))
* set update diff after to array for identifying array changes ([2d540bf](https://gitlab.com/awcjack/deep-object-diff-array/commit/2d540bfc8964c98cff3eeccc4d04e68bb0d0dfbe))
* update reorder testcase ([53a95c7](https://gitlab.com/awcjack/deep-object-diff-array/commit/53a95c75a42dfe9906afaf9cdeb6da1476c9c541))


### Features

* root array handling and updated array handling ([3282490](https://gitlab.com/awcjack/deep-object-diff-array/commit/3282490597ddc4a9cf5d716e058b36b870dc229d))
* simple array and reorder fix ([7a1f675](https://gitlab.com/awcjack/deep-object-diff-array/commit/7a1f67559ca37dffb8dd7a788fe932f674687f77))
